package imageattributereader.bitmaps;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;

import javax.imageio.ImageIO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import imageattributereader.bitmaps.Bitmap;
import imageattributereader.bitmaps.BitmapBuilder;

@RunWith(Parameterized.class)
public class BitmapBuilderTests {

    private final static String currentDir = System.getProperty("user.dir");

    @Parameterized.Parameters
    public static Collection<Object[]> data() {

        // path to input image file, expected bitmap object
        Object[][] data = new Object[][] {
                { currentDir + "/src/test/resources/s1/1.bmp",
                        currentDir + "/src/test/resources/s1/1.bmp" },
                { currentDir + "/src/test/resources/s1/2.bmp",
                        currentDir + "/src/test/resources/s1/2.bmp" },
                { currentDir + "/src/test/resources/s1/3.bmp",
                        currentDir + "/src/test/resources/s1/3.bmp" },
                { currentDir + "/src/test/resources/s1/4.bmp",
                        currentDir + "/src/test/resources/s1/4.bmp" },
                { currentDir + "/src/test/resources/s1/5.bmp",
                        currentDir + "/src/test/resources/s1/5.bmp" },
                { currentDir + "/src/test/resources/s1/6.bmp",
                        currentDir + "/src/test/resources/s1/6.bmp" },
                { currentDir + "/src/test/resources/s1/7.bmp",
                        currentDir + "/src/test/resources/s1/7.bmp" },
                { currentDir + "/src/test/resources/s1/8.bmp",
                        currentDir + "/src/test/resources/s1/8.bmp" },
                { currentDir + "/src/test/resources/s1/9.bmp",
                        currentDir + "/src/test/resources/s1/9.bmp" },
                { currentDir + "/src/test/resources/s1/10.bmp",
                        currentDir + "/src/test/resources/s1/10.bmp" } };

        return Arrays.asList(data);
    }

    private Bitmap bitmap;

    private Bitmap expectedBitmap;

    public BitmapBuilderTests(String path, String expectedBitmap)
            throws IOException {
        byte[] byteArray = Files.readAllBytes(Paths.get(path));
        this.bitmap = BitmapBuilder.fromByteArray(byteArray);
        this.expectedBitmap = new Bitmap(ImageIO.read(new File(expectedBitmap)));
    }

    @Test
    public void testBitmapIsNotNull() {
        assertThat(bitmap, is(notNullValue()));
    }

    @Test
    public void testBitmapIsValid() {
        assertThat(
                bitmap.getImage().getData().getDataBuffer().getElem(50),
                is(expectedBitmap.getImage().getData().getDataBuffer()
                        .getElem(50)));
    }

    @Test(
            expected = IllegalArgumentException.class)
    public void testThrowsIfPathIsNull() {
        BitmapBuilder.fromByteArray(null);
    }
}
