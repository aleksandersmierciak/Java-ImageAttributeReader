package imageattributereader.bitmaps;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.awt.Dimension;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class BitmapPartTests {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {

        // path to image resource, division direction,
        // expected first part, expected second part, expected attribute value
        Object[][] data = new Object[][] {
                {
                        System.getProperty("user.dir")
                                + "/src/test/resources/s1/image4x4.bmp",
                        Direction.HORIZONTAL,
                        BitmapBuilder
                                .fromPath(System.getProperty("user.dir")
                                        + "/src/test/resources/s1/image2x1_horizontal_first.bmp"),
                        BitmapBuilder
                                .fromPath(System.getProperty("user.dir")
                                        + "/src/test/resources/s1/image2x1_horizontal_second.bmp"),
                        251 },
                {
                        System.getProperty("user.dir")
                                + "/src/test/resources/s1/image4x4.bmp",
                        Direction.VERTICAL,
                        BitmapBuilder
                                .fromPath(System.getProperty("user.dir")
                                        + "/src/test/resources/s1/image1x2_vertical_first.bmp"),
                        BitmapBuilder
                                .fromPath(System.getProperty("user.dir")
                                        + "/src/test/resources/s1/image1x2_vertical_second.bmp"),
                        251 } };

        return Arrays.asList(data);
    }

    private final BitmapPart bitmapPart;

    private final Direction direction;

    private final int expectedAttribute;

    private final Bitmap expectedFirst;

    private final Bitmap expectedSecond;

    public BitmapPartTests(String path, Direction direction,
            Bitmap expectedFirst, Bitmap expectedSecond, int expectedAttribute) {
        Bitmap bitmap = (BitmapBuilder.fromPath(path));

        bitmap.divide(new Dimension(2, 2), 1);
        this.bitmapPart = new BitmapPart(bitmap.getBitmapParts().get(4)
                .getImage());
        this.direction = direction;
        this.bitmapPart.divide(direction);
        this.expectedFirst = expectedFirst;
        this.expectedSecond = expectedSecond;
        this.expectedAttribute = expectedAttribute;
    }

    @Test
    public void testAttributeIsNotNull() {
        assertThat(bitmapPart.getAttribute(), is(notNullValue()));
    }

    @Test
    public void testAttributeIsValid() {
        assertThat(bitmapPart.getAttribute(), is(expectedAttribute));
    }

    @Test
    public void testDivisionFirstResultIsNotNull() {
        assertThat(bitmapPart.getFirst(), is(notNullValue()));
    }

    @Test
    public void testDivisionFirstResultIsValid() {
        assertThat(
                bitmapPart.getFirst().getImage().getRaster()
                        .getPixel(0, 0, new int[3]), is(expectedFirst
                        .getImage().getRaster().getPixel(0, 0, new int[3])));
    }

    @Test
    public void testDivisionSecondResultIsNotNull() {
        assertThat(bitmapPart.getSecond(), is(notNullValue()));
    }

    @Test
    public void testDivisionSecondResultIsValid() {

        if (direction.equals(Direction.HORIZONTAL)) {
            assertThat(
                    bitmapPart.getSecond().getImage().getRaster()
                            .getPixel(1, 0, new int[3]), is(expectedSecond
                            .getImage().getRaster().getPixel(1, 0, new int[3])));
        }

        if (direction.equals(Direction.VERTICAL)) {
            assertThat(
                    bitmapPart.getSecond().getImage().getRaster()
                            .getPixel(0, 1, new int[3]), is(expectedSecond
                            .getImage().getRaster().getPixel(0, 1, new int[3])));
        }
    }

    @Test(
            expected = IllegalArgumentException.class)
    public void testThrowsIfDirectionIsNull() {
        bitmapPart.divide(null);
    }

    @Test(
            expected = IllegalArgumentException.class)
    public void testThrowsIfInputIsNull() {
        new BitmapPart(null);
    }
}
