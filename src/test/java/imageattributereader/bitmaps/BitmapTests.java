package imageattributereader.bitmaps;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.awt.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class BitmapTests {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {

        // input byte array
        Object[][] data = new Object[][] { {
                new byte[] { 66, 77, 70, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0, 40,
                        0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 1, 0, 24, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, -1, -1, -1, 3, 3, 3, 0, 0, -1, -1, -1, -1, -1,
                        -1, 0, 0 },
                System.getProperty("user.dir")
                        + "/src/test/resources/s1/image2x2.bmp" } };

        return Arrays.asList(data);
    }

    private final Bitmap bitmap;

    private final Bitmap expectedBitmap;

    private final List<Bitmap> expectedBitmapParts;

    public BitmapTests(byte[] input, String path) {
        this.bitmap = BitmapBuilder.fromByteArray(input);
        this.expectedBitmap = BitmapBuilder.fromPath(path);
        this.expectedBitmapParts = new ArrayList<Bitmap>();
        this.expectedBitmapParts.add(BitmapBuilder.fromByteArray(new byte[] {
                66, 77, 58, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0, 40, 0, 0, 0, 1,
                0, 0, 0, 1, 0, 0, 0, 1, 0, 24, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1, 0 }));
        this.expectedBitmapParts.add(BitmapBuilder.fromByteArray(new byte[] {
                66, 77, 58, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0, 40, 0, 0, 0, 1,
                0, 0, 0, 1, 0, 0, 0, 1, 0, 24, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1, 0 }));
        this.expectedBitmapParts.add(BitmapBuilder.fromByteArray(new byte[] {
                66, 77, 58, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0, 40, 0, 0, 0, 1,
                0, 0, 0, 1, 0, 0, 0, 1, 0, 24, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1, 0 }));
        this.expectedBitmapParts.add(BitmapBuilder.fromByteArray(new byte[] {
                66, 77, 58, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0, 40, 0, 0, 0, 1,
                0, 0, 0, 1, 0, 0, 0, 1, 0, 24, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 0 }));
    }

    @Test
    public void testBitmapPartsAreNotNull() {
        bitmap.divide(new Dimension(1, 1), 1);
        assertThat(bitmap.getBitmapParts(), is(notNullValue()));
    }

    @Test
    public void testBitmapPartsAreValid() throws IOException {
        bitmap.divide(new Dimension(1, 1), 1);

        assertThat(bitmap.getBitmapParts().get(3).getImage().getRaster()
                .getPixel(0, 0, new int[3]), is(expectedBitmapParts.get(3)
                .getImage().getRaster().getPixel(0, 0, new int[3])));
    }

    @Test
    public void testImageIsNotNull() {
        assertThat(bitmap.getImage(), is(notNullValue()));
    }

    @Test
    public void testImageIsValid() {
        assertThat(
                bitmap.getImage().getData().getDataBuffer().getElem(1),
                is(expectedBitmap.getImage().getData().getDataBuffer()
                        .getElem(1)));
    }

    @Test(
            expected = IllegalArgumentException.class)
    public void testThrowsIfDimensionIsNull() {
        bitmap.divide(null, 1);
    }

    @Test(
            expected = IllegalArgumentException.class)
    public void testThrowsIfHeightIsNegative() {
        bitmap.divide(new Dimension(1, -1), 1);
    }

    @Test(
            expected = IllegalArgumentException.class)
    public void testThrowsIfHeightIsZero() {
        bitmap.divide(new Dimension(1, 0), 1);
    }

    @Test(
            expected = IllegalArgumentException.class)
    public void testThrowsIfOffsetIsNegative() {
        bitmap.divide(new Dimension(1, 1), -1);
    }

    @Test(
            expected = IllegalArgumentException.class)
    public void testThrowsIfOffsetIsOutOfBound() {
        bitmap.divide(new Dimension(1, 1), 99999);
    }

    @Test(
            expected = IllegalArgumentException.class)
    public void testThrowsIfOffsetIsZero() {
        bitmap.divide(new Dimension(1, 1), 0);
    }

    @Test(
            expected = IllegalArgumentException.class)
    public void testThrowsIfWidthIsNegative() {
        bitmap.divide(new Dimension(-1, 1), 1);
    }

    @Test(
            expected = IllegalArgumentException.class)
    public void testThrowsIfWidthIsZero() {
        bitmap.divide(new Dimension(0, 1), 1);
    }
}
