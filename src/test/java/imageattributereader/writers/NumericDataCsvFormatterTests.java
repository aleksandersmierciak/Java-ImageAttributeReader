package imageattributereader.writers;

import imageattributereader.collections.IntegerData;
import imageattributereader.collections.NumericData;
import imageattributereader.writers.NumericDataFormatter;
import imageattributereader.writers.NumericDataCsvFormatter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;

@RunWith(Parameterized.class)
public class NumericDataCsvFormatterTests {
    private final NumericDataFormatter formatter;

    private final String expectedOutput;

    private final String actualOutput;

    public NumericDataCsvFormatterTests(NumericData numericData, String expectedOutput) {
        this.formatter = new NumericDataCsvFormatter();
        this.expectedOutput = expectedOutput;
        this.actualOutput = formatter.formatData(numericData);
    }

    @Parameterized.Parameters
    public static List<Object[]> data() {
        Object[][] data = new Object[][] {
                { new IntegerData("Title1", Arrays.asList(0)), "Title1;0" },
                { new IntegerData("Title2", Arrays.asList(0, 0)), "Title2;0;0" },
                { new IntegerData("Title3", Arrays.asList(34633834, 5684563)), "Title3;34633834;5684563" },
        };
        return Arrays.asList(data);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testThrowsIfNumericDataIsNull() {
        formatter.formatData(null);
    }

    @Test
    public void testOutputIsNotNull() {
        assertThat(actualOutput, is(notNullValue()));
    }

    @Test
    public void testOutputIsNotEmpty() {
        assertThat(actualOutput, is(not("")));
    }

    @Test
    public void testOutputIsValid() {
        assertThat(actualOutput, is(equalTo(expectedOutput)));
    }
}
