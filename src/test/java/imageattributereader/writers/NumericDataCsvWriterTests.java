package imageattributereader.writers;

import imageattributereader.collections.IntegerData;
import imageattributereader.collections.NumericData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

@RunWith(Parameterized.class)
public class NumericDataCsvWriterTests {
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        NumericData data1 = new IntegerData("Title1", Arrays.asList(0)); // "Title1;0"
        NumericData data2 = new IntegerData("Title2", Arrays.asList(0, 0)); // "Title2;0;0"
        NumericData data3 = new IntegerData("Title3", Arrays.asList(1, 2)); // "Title3;1;2"
        NumericData data4 = new IntegerData("Title4", Arrays.asList(34633834, 5684563)); // "Title4;34633834;5684563"
        NumericData data5 = new IntegerData("Title5", Arrays.asList(-0)); // "Title5;-0"
        NumericData data6 = new IntegerData("Title5", Arrays.asList(-0, -0)); // "Title6;-0;-0"
        NumericData data7 = new IntegerData("Title6", Arrays.asList(-1, -2)); // "Title7;-1;-2"
        NumericData data8 = new IntegerData("Title7", Arrays.asList(-34633834, -5684563)); // "Title8;-34633834;-5684563"

        Object[][] data = new Object[][] {
                {Arrays.asList(data1), "Title1;0"},
                {Arrays.asList(data1, data2), "Title1;0\nTitle2;0;0\n"},
                {Arrays.asList(data1, data2, data3), "Title1;0\nTitle2;0;0\nTitle3;1;2\n"},
                {Arrays.asList(data1, data2, data3, data4), "Title1;0\nTitle2;0;0\nTitle3;1;2\nTitle4;34633834;5684563\n"},
                {Arrays.asList(data5), "Title5;-0"},
                {Arrays.asList(data5, data6), "Title5;-0\nTitle6;-0;-0\n"},
                {Arrays.asList(data5, data6, data7), "Title5;-0\nTitle6;-0;-0\nTitle7;-1;-2\n"},
                {Arrays.asList(data5, data6, data7, data8), "Title5;-0\nTitle6;-0;-0\nTitle7;-1;-2\nTitle8;-34633834;-5684563\n"},
        };

        return Arrays.asList(data);
    }

    private final String actualOutput;

    private final String expectedOutput;

    public NumericDataCsvWriterTests(Iterable<IntegerData> input, String expectedOutput) {
        NumericDataWriter writer = new NumericDataCsvWriter();
        this.actualOutput = writer.write(input);
        this.expectedOutput = expectedOutput;
    }

    @Test
    public void testOutputIsNotNull() {
        assertThat(expectedOutput, is(notNullValue()));
    }

    @Test
    public void testOutputIsValid() {
        assertThat(actualOutput, is(equalTo(actualOutput)));
    }
}
