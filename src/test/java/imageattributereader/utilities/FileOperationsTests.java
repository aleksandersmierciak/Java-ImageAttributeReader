package imageattributereader.utilities;

import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;

public class FileOperationsTests {
    @Test(expected = IllegalArgumentException.class)
    public void testThrowsIfFileToSaveIsNull() throws FileNotFoundException {
        FileOperations.saveToFile(null, "Test");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testThrowsIfDataToSaveIsNull() throws FileNotFoundException {
        FileOperations.saveToFile(new File("Some test file"), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testThrowsIfDataToSaveIsEmpty() throws FileNotFoundException {
        FileOperations.saveToFile(new File("Some test file"), "");
    }
}
