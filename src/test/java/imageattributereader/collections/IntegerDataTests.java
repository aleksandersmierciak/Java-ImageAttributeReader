package imageattributereader.collections;

import java.util.ArrayList;

import org.junit.Test;

public class IntegerDataTests {
    @Test(
            expected = IllegalArgumentException.class)
    public void testThrowsIfCollectionTitleIsEmpty() {
        new IntegerData("", new ArrayList<Integer>());
    }

    @Test(
            expected = IllegalArgumentException.class)
    public void testThrowsIfCollectionTitleIsNull() {
        new IntegerData(null, new ArrayList<Integer>());
    }

    @Test(
            expected = IllegalArgumentException.class)
    public void testThrowsIfNumericDataIsNull() {
        new IntegerData("Title", null);
    }
}
