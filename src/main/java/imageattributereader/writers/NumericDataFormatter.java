package imageattributereader.writers;

import imageattributereader.collections.NumericData;

public interface NumericDataFormatter {
    String formatData(NumericData data);
}
