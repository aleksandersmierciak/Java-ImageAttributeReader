package imageattributereader.writers;

import imageattributereader.collections.NumericData;

public class NumericDataCsvFormatter implements NumericDataFormatter {
    private static final char CSV_SEPARATOR = ';';

    @Override
    public String formatData(NumericData data) {
        if (data == null) {
            throw new IllegalArgumentException("Numeric data cannot be null");
        }

        StringBuilder builder = new StringBuilder();
        builder.append(data.getCollectionTitle());
        Iterable<Number> numbers = data.getNumericData();
        for (Number number : numbers) {
            builder.append(CSV_SEPARATOR);
            builder.append(number);
        }

        return builder.toString();
    }
}
