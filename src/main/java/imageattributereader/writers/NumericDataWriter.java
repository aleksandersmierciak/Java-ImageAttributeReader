package imageattributereader.writers;

import imageattributereader.collections.IntegerData;

public interface NumericDataWriter {
    String write(Iterable<IntegerData> data);
}
