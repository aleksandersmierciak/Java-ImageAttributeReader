package imageattributereader.writers;

import imageattributereader.collections.IntegerData;

import java.util.List;

/**
 * Class responsible for creating CSV file to store information about image
 * attributes.
 */
public class NumericDataCsvWriter implements NumericDataWriter {
    /**
     * Writes given data ({@link List}) to simple string.
     * 
     * @param data
     *            Collected data as the list of attributes.
     * @return the simple string to save.
     */
    @Override
    public String write(Iterable<IntegerData> data) {
        if (data == null) {
            throw new IllegalArgumentException("Data cannot be null");
        }

        StringBuilder stringBuilder = new StringBuilder();
        NumericDataFormatter formatter = new NumericDataCsvFormatter();

        for (IntegerData integerData : data) {
            stringBuilder.append(formatter.formatData(integerData));
            stringBuilder.append('\n');
        }
        return stringBuilder.toString();
    }
}
