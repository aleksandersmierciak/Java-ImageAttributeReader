/**
 * Project Java-ImageAttributeReader.
 * Copyright Michał Szczygieł & Aleksander Śmierciak
 * Created at Aug 14, 2014.
 */
package imageattributereader.bitmaps;

/**
 * Enumerable represents directions.
 * 
 * @author m4gik <michal.szczygiel@wp.pl>
 * 
 */
public enum Direction {
    HORIZONTAL, VERTICAL
}
