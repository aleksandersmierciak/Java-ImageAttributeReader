package imageattributereader.bitmaps;
/**
 * Project Java-ImageAttributeReader.
 * Copyright Michał Szczygieł & Aleksander Śmierciak
 * Created at June 25, 2014.
 */


/**
 * Interface to represents behavior of picture instance.
 * 
 * @author m4gik <michal.szczygiel@wp.pl>
 */
public interface Picture {

    /**
     * Gets the object that produces the pixels for the image. This method is
     * called by the image filtering classes and by methods that perform image
     * conversion and scaling.
     * 
     * @return the image instance represented in matrix of pixel
     */
    Object getContent();

    /**
     * Determines the height of the image. If the height is not yet known, this
     * method returns <code>-1</code>.
     * 
     * @return the height of this image, or <code>-1</code> if the height is not
     *         yet known.
     */
    Integer getHeight();

    /**
     * Gets a property of this image by name.
     * <p/>
     * Individual property names are defined by the various image formats. If a
     * property is not defined for a particular image, this method returns the
     * <code>UndefinedProperty</code> object.
     * <p/>
     * The property name <code>"comment"</code> should be used to store an
     * optional comment which can be presented to the application as a
     * description of the image, its source, or its author.
     * 
     * @param name
     *            a property name.
     * @return the value of the named property.
     * @throws NullPointerException
     *             if the property name is null.
     */
    Object getProperty(String name) throws NullPointerException;

    /**
     * Determines the width of the image. If the width is not yet known, this
     * method returns <code>-1</code>.
     * 
     * @return the width of this image, or <code>-1</code> if the width is not
     *         yet known.
     */
    Integer getWidth();
}
