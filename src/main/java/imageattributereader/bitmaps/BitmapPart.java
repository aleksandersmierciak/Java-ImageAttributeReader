/**
 * Project Java-ImageAttributeReader.
 * Copyright Michał Szczygieł & Aleksander Śmierciak
 * Created at Aug 14, 2014.
 */

package imageattributereader.bitmaps;

import java.awt.image.BufferedImage;
import java.lang.reflect.Constructor;

/**
 * Class responsible for representative part of bitmap image properties.
 * 
 * @author m4gik <michal.szczygiel@wp.pl>
 */
public class BitmapPart extends Bitmap {

    private Integer attribute;

    private BitmapPart first;

    private BitmapPart second;

    /**
     * The {@link Constructor} of {@link BitmapPart}.
     * 
     * @param image
     *            The instance of {@link BufferedImage}.
     */
    public BitmapPart(BufferedImage image) {
        super(image);
    }

    /**
     * Divides the current {@link BitmapPart} to two pieces depends on the
     * direction. Also this method performs attribute calculation.
     * 
     * @param direction
     *            The direction of cut.
     */
    public void divide(Direction direction) {

        if (direction == null) {
            throw new IllegalArgumentException();
        }

        if (direction.equals(Direction.HORIZONTAL)) {
            first = new BitmapPart(getImage().getSubimage(0, 0, getWidth(),
                    getHeight() / 2));
            second = new BitmapPart(getImage().getSubimage(0, getHeight() / 2,
                    getWidth(), getHeight() / 2));
        }

        if (direction.equals(Direction.VERTICAL)) {
            first = new BitmapPart(getImage().getSubimage(0, 0, getWidth() / 2,
                    getHeight()));
            second = new BitmapPart(getImage().getSubimage(getWidth() / 2, 0,
                    getWidth() / 2, getHeight()));
        }

        // Operations for counting the attribute.
        Integer sumOfFirst = 0;

        for (int y = 0; y < first.getHeight(); y++) {
            for (int x = 0; x < first.getWidth(); x++) {
                sumOfFirst += first.getImage().getRaster().getSample(x, y, 0);
            }
        }

        Integer sumOfSecond = 0;

        for (int y = 0; y < second.getHeight(); y++) {
            for (int x = 0; x < second.getWidth(); x++) {
                sumOfSecond += second.getImage().getRaster().getSample(x, y, 0);
            }
        }

        attribute = sumOfFirst - sumOfSecond;
    }

    /**
     * Gets the attribute value as a subtract of two part of {@link BitmapPart}.
     * 
     * @return the attribute value after divide operation.
     */
    public Integer getAttribute() {
        return attribute;
    }

    /**
     * Gets the first part of bitmap after divide operation.
     * 
     * @return the first part of {@link BitmapPart}.
     */
    public BitmapPart getFirst() {
        return first;
    }

    /**
     * Gets the second part of bitmap after divide operation.
     * 
     * @return the second part of {@link BitmapPart}.
     */
    public BitmapPart getSecond() {
        return second;
    }

}
