/**
 * Project Java-ImageAttributeReader.
 * Copyright Michał Szczygieł & Aleksander Śmierciak
 * Created at Aug 12, 2014.
 */

package imageattributereader.bitmaps;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * Class responsible for building Bitmap.
 * 
 * @author m4gik <michal.szczygiel@wp.pl>
 * 
 */
public class BitmapBuilder {

    /**
     * Creates new instance of {@link Bitmap} from byte array.
     * 
     * @param byteArray
     *            The byte array to create bitmap instance.
     * @return the instance of {@link Bitmap}.
     */
    public static Bitmap fromByteArray(byte[] byteArray) {

        if (byteArray == null) {
            throw new IllegalArgumentException();
        }

        try {
            return new Bitmap(ImageIO.read(new ByteArrayInputStream(byteArray)));
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Creates new instance of {@link Bitmap} from file.
     * 
     * @param bitmapFile
     *            The file of Bitmap.
     * @return the instance of {@link Bitmap}.
     */
    public static Bitmap fromFile(File bitmapFile) {

        if (bitmapFile == null) {
            throw new IllegalArgumentException();
        }

        try {
            return new Bitmap(ImageIO.read(bitmapFile));
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Creates new instance of {@link Bitmap} from given directory.
     * 
     * @param path
     *            The path to bitmap file.
     * @return the instance of {@link Bitmap}.
     */
    public static Bitmap fromPath(String path) {

        if (path == null) {
            throw new IllegalArgumentException();
        }

        try {
            return new Bitmap(ImageIO.read(new File(path)));
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

}
