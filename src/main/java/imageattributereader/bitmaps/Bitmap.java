/**
 * Project Java-ImageAttributeReader.
 * Copyright Michał Szczygieł & Aleksander Śmierciak
 * Created at June 25, 2014.
 */

package imageattributereader.bitmaps;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 * Class responsible for representative Bitmap image properties.
 * 
 * @author m4gik <michal.szczygiel@wp.pl>
 */
public class Bitmap implements Picture {

    private List<BitmapPart> bitmapParts = null;

    private Graphics2D content;

    private Integer height = -1;

    private BufferedImage image;

    private Object property;

    private Integer width = -1;

    /**
     * The constructor for {@link Bitmap} class.
     * 
     * @param image
     *            The instance of {@link BufferedImage}.
     */
    public Bitmap(final BufferedImage image) {

        if (image == null) {
            throw new IllegalArgumentException();
        }

        this.image = image;
        this.height = image.getHeight();
        this.width = image.getWidth();
        this.property = image;
        this.content = (Graphics2D) image.getGraphics();
    }

    /**
     * This method cut the point {@link BitmapPart} by offset to smaller bitmaps
     * according to the specified size.
     * 
     * @param dimension
     *            Size of trim the bitmap.
     * @param offset
     *            Indicating the distance (displacement) from the beginning of
     *            the object up until a given element or point, within the same
     *            object.
     */
    public void divide(Dimension dimension, Integer offset) {

        if (dimension == null) {
            throw new IllegalArgumentException();
        }

        if (dimension.getHeight() <= 0 || dimension.getWidth() <= 0
                || offset <= 0) {
            throw new IllegalArgumentException();
        }

        // To prevent to moving larger distance than the picture size
        if (dimension.getHeight() >= getHeight()) {
            throw new IllegalArgumentException();
        }

        if (dimension.getWidth() >= getWidth()) {
            throw new IllegalArgumentException();
        }

        if (dimension.getHeight() + offset > getHeight()
                || dimension.getWidth() + offset > getWidth()) {
            throw new IllegalArgumentException();
        }

        // The part for divide bitmap
        int currentPositionX = 0;
        int currentPositionY = 0;
        int maxX = (int) Math.floor((getWidth() - dimension.getWidth())
                / offset);
        int maxY = (int) Math.floor((getHeight() - dimension.getHeight())
                / offset);

        bitmapParts = new ArrayList<BitmapPart>();

        for (int j = 0; j < maxY + 1; j++) {
            for (int i = 0; i < maxX + 1; i++) {
                bitmapParts.add(new BitmapPart(getImage()
                        .getSubimage(currentPositionX, currentPositionY,
                                (int) dimension.getWidth(),
                                (int) dimension.getHeight())));
                currentPositionX += offset;
            }
            currentPositionX = 0;
            currentPositionY += offset;
        }

    }

    /**
     * Gets the all parts of {@link Bitmap} instance.
     * 
     * @return the bitmap parts as a {@link List}.
     */
    public List<BitmapPart> getBitmapParts() {
        return bitmapParts;
    }

    /**
     * Gets the object that produces the pixels for the image. This method is
     * called by the image filtering classes and by methods that perform image
     * conversion and scaling.
     * 
     * @return the image instance represented in matrix of pixel
     */
    public Graphics2D getContent() {
        return this.content;
    }

    /**
     * Determines the height of the image. If the height is not yet known, this
     * method returns <code>-1</code>.
     * 
     * @return the height of this image, or <code>-1</code> if the height is not
     *         yet known.
     */
    public Integer getHeight() {
        return this.height;
    }

    /**
     * Gets image properties of bitmap as BufferedImage.
     * 
     * @return the image of current bitmap.
     */
    public BufferedImage getImage() {
        return this.image;
    }

    /**
     * Gets a property of this image by name.
     * <p/>
     * Individual property names are defined by the various image formats. If a
     * property is not defined for a particular image, this method returns the
     * <code>UndefinedProperty</code> object.
     * <p/>
     * The property name <code>"comment"</code> should be used to store an
     * optional comment which can be presented to the application as a
     * description of the image, its source, or its author.
     * 
     * @param name
     *            a property name.
     * @return the value of the named property.
     * @throws NullPointerException
     *             if the property name is null.
     */
    public Object getProperty(final String name) throws NullPointerException {
        return ((BufferedImage) property).getProperty(name);
    }

    /**
     * Determines the width of the image. If the width is not yet known, this
     * method returns <code>-1</code>.
     * 
     * @return the width of this image, or <code>-1</code> if the width is not
     *         yet known.
     */
    public Integer getWidth() {
        return this.width;
    }

}
