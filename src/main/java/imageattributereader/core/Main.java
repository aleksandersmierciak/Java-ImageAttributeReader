/**
 * Project Java-ImageAttributeReader.
 * Copyright Michał Szczygieł & Aleksander Śmierciak
 * Created at Aug 14, 2014.
 */

package imageattributereader.core;

import imageattributereader.bitmaps.Bitmap;
import imageattributereader.bitmaps.BitmapPart;
import imageattributereader.bitmaps.Direction;
import imageattributereader.collections.IntegerData;
import imageattributereader.utilities.FileOperations;
import imageattributereader.writers.NumericDataCsvWriter;

import java.awt.Dimension;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.LogManager;

/**
 * The main class of application.
 * 
 * @author m4gik <michal.szczygiel@wp.pl>
 */
public class Main {

    private static Direction DIRECTION = Direction.HORIZONTAL;

    /**
     * Collects data from image collection.
     * 
     * @param bitmapCollection
     *            The data to process.
     * @param dimension
     *            The dimension of box to collect information.
     * @param offset
     *            The step of box to collect information.
     */
    private static Iterable<IntegerData> bitmapProcess(
            Map<String, List<Bitmap>> bitmapCollection, Dimension dimension,
            int offset) {
        List<IntegerData> data = new ArrayList<IntegerData>();

        for (Map.Entry<String, List<Bitmap>> entry : bitmapCollection
                .entrySet()) {
            for (Bitmap bitmap : entry.getValue()) {
                bitmap.divide(dimension, offset);
                List<Integer> attributes = new ArrayList<Integer>();
                for (BitmapPart bitmapPart : bitmap.getBitmapParts()) {
                    bitmapPart.divide(DIRECTION);
                    attributes.add(bitmapPart.getAttribute());
                }
                data.add(new IntegerData(entry.getKey(), attributes));
            }
        }

        return data;
    }

    /**
     * Checks the correctness of arguments.
     * 
     * @param args
     *            Arguments to check.
     */
    private static void checkArguments(String[] args) {
        if (args.length != 6) {
            throw new IllegalArgumentException(
                    "The applications needs 6 parameters: "
                            + "\n-path to image resources,"
                            + "\n-width of box which gather information about attribute,"
                            + "\n-height of box which gather information about attribute,"
                            + "\n-offset what it means step which moves the box,"
                            + "\n-direction, if value is \"h\" then direction is horizontal, if \"v\" then vertical "
                            + "\n-the name of file result.");
        }

        if (Integer.parseInt(args[1]) < 1) {
            throw new IllegalArgumentException(
                    "The width of box to collect information should be greater than 0");
        }

        if (Integer.parseInt(args[2]) < 1) {
            throw new IllegalArgumentException(
                    "The height of box to collect information should be greater than 0");
        }

        if (Integer.parseInt(args[3]) < 1) {
            throw new IllegalArgumentException(
                    "Offset should be greater than 0");
        }

        if (!(args[4].charAt(0) == 'h' || args[4].charAt(0) == 'v')) {
            throw new IllegalArgumentException("Direction should be h or v");
        }

    }

    /**
     * Main method. Executing and solving the problem. Should have only five
     * parameters:
     * 
     * -path to image resources,
     * 
     * -width of box which gather information about attribute,
     * 
     * -height of box which gather information about attribute,
     * 
     * -offset what it means step which moves the box,
     * 
     * -direction for cut,
     * 
     * -the name of file result.
     * 
     * @param args
     * @throws FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
        setupLogger(false);
        checkArguments(args);
        setDirection(args[4].charAt(0));
        Map<String, List<Bitmap>> bitmapCollection = FileOperations
                .loadImages(args[0]);
        Iterable<IntegerData> dataToSave = bitmapProcess(
                bitmapCollection,
                new Dimension(Integer.parseInt(args[1]), Integer
                        .parseInt(args[2])), Integer.parseInt(args[3]));
        FileOperations.saveToFile(new File(args[5]),
                new NumericDataCsvWriter().write(dataToSave));
    }

    /**
     * Sets direction to scan attributes of image.
     * 
     * @param option
     *            The value "h" for horizontal or "v" for vertical.
     */
    private static void setDirection(char option) {
        switch (option) {
        case 'h':
            DIRECTION = Direction.HORIZONTAL;
            break;
        case 'v':
            DIRECTION = Direction.VERTICAL;
            break;
        }
    }

    /**
     * Method responsible for turn off or turn on logging for application.
     * 
     * @param value
     */
    private static void setupLogger(Boolean value) {
        if (value == false) {
            LogManager.getLogManager().reset();
        }
    }

}
