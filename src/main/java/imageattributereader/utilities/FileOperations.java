package imageattributereader.utilities;

import imageattributereader.bitmaps.Bitmap;
import imageattributereader.bitmaps.BitmapBuilder;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class FileOperations {

    private final static FileFilter imagesFilesOnly = new FileFilter() {
        @Override
        public boolean accept(File pathname) {
            for (String fileExtension : imageFileExtensions) {
                if (pathname.getName().toLowerCase().endsWith(fileExtension)) {
                    return true;
                }
            }

            return false;
        }
    };

    private final static FileFilter directoriesOnly = new FileFilter() {
        @Override
        public boolean accept(File pathname) {
            return pathname.isDirectory();
        }
    };

    private final static String[] imageFileExtensions = new String[] { "jpg",
            "png", "gif", "bmp" };

    private final static Logger LOGGER = Logger.getLogger(FileOperations.class
            .getName());

    /**
     * Loads images from given path.<br>
     * Subdirectories are marked as labels.<br>
     * Bitmap files from subdirectories are loaded.<br>
     * 
     * @param folderPath
     *            path to image resources
     * @return map of faces where key is folder label
     */
    public static Map<String, List<Bitmap>> loadImages(String folderPath) {
        LOGGER.info("Searching for files:");

        Map<String, List<Bitmap>> map = new LinkedHashMap<String, List<Bitmap>>();
        File src = new File(folderPath);

        LOGGER.info(src.getName() + ":");

        if (src.isDirectory()) {

            File[] subdirectories = src.listFiles(directoriesOnly);

            for (File file : subdirectories) {
                LOGGER.info(src.getName() + "/" + file.getName() + ":");

                List<Bitmap> list = new ArrayList<Bitmap>();
                String name = file.getName();
                File[] imageFiles = file.listFiles(imagesFilesOnly);

                for (File imageFile : imageFiles) {
                    LOGGER.info(src.getName() + "/" + file.getName() + "/"
                            + imageFile.getName());
                    Bitmap image = BitmapBuilder.fromFile(imageFile);
                    list.add(image);
                }

                map.put(name, list);
            }
        } else {
            LOGGER.warning("ERROR: Invalid Directory");
        }

        return map;
    }

    /**
     * Saves data to file.
     * 
     * @param destination
     *            The file to save data.
     * @param data
     *            The data to save.
     */
    public static void saveToFile(File destination, String data)
            throws FileNotFoundException {
        if (destination == null) {
            throw new IllegalArgumentException("File cannot be null");
        }

        if (data == null) {
            throw new IllegalArgumentException("Data to save cannot be null");
        }

        if (data.equals("")) {
            throw new IllegalArgumentException("Data to save cannot be empty");
        }

        PrintStream out = new PrintStream(new FileOutputStream(destination));
        out.print(data);
        out.flush();
        out.close();

        LOGGER.info("File was successfully saved in " + destination);
    }
}
