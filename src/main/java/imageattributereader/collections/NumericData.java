package imageattributereader.collections;

public interface NumericData<T extends Number> {
    String getCollectionTitle();

    Iterable<T> getNumericData();
}
