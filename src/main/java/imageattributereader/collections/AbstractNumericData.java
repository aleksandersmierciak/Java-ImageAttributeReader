package imageattributereader.collections;

public abstract class AbstractNumericData<T extends Number> implements NumericData<T> {
    private final String collectionTitle;

    private final Iterable<T> numericData;

    public AbstractNumericData(String collectionTitle, Iterable<T> numericData) {
        if (collectionTitle == null) {
            throw new IllegalArgumentException("Collection title cannot be null");
        }

        if (collectionTitle.equals("")) {
            throw new IllegalArgumentException("Collection title cannot be empty");
        }

        if (numericData == null) {
            throw new IllegalArgumentException("Numeric data cannot be null");
        }

        this.collectionTitle = collectionTitle;
        this.numericData = numericData;
    }

    @Override
    public String getCollectionTitle() {
        return collectionTitle;
    }

    @Override
    public Iterable<T> getNumericData() {
        return numericData;
    }
}
