package imageattributereader.collections;

public class IntegerData extends AbstractNumericData<Integer> {
    public IntegerData(String collectionTitle, Iterable<Integer> numericData) {
        super(collectionTitle, numericData);
    }
}
